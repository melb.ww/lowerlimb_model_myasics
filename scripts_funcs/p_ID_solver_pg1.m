% clear all

import org.opensim.modeling.*

File_model='C:\OpenSim_4\model_1\Models\Gait2392_Simbody_Tim_Sayer\gait2392_simbody_TimSayer_JRF_shift_M.osim';
File_motion='C:\OpenSim_4\model_1\Models\Gait2392_Simbody_Tim_Sayer\Subj_spec_model_data\Dev36\Barefoot\Dev36_Running_03_cut.mot';
my_sto=Storage(File_motion);
my_GCV=GCVSplineSet(5,my_sto);




File_GRF=file_grf;

my_model=Model(File_model);
myState =  my_model.initSystem();
idSolver = InverseDynamicsSolver(my_model);

% ----coord details
my_coor_set=my_model.getCoordinateSet();
nCoord_all = my_coor_set.getSize();
my_coord_names=cell(nCoord_all,1);
for n=1:nCoord_all
    my_coord_names(n)=my_coor_set.get(n-1).getName();
end


%%
[my_mot_imp,nFrame_mot,nCoord_mot] = f_mot_read(File_motion);

%%
setup_forceplate_LR={...
    {'r','1'},...
    {'l','3'},...
    };
extLoadsObject = ExternalLoads();
extLoadsObject.setName('ExLoad_API');
extLoadsObject.setDataFileName(File_GRF);
extLoadsObject.setLowpassCutoffFrequencyForLoadKinematics(6);
for ind=1:length(setup_forceplate_LR)
    LR=setup_forceplate_LR{ind}{1};
    Num_plate=setup_forceplate_LR{ind}{2};
    
    temp_ext_force=ExternalForce;
    temp_ext_force.setName(['ex_force_',LR]);
    temp_ext_force.set_applied_to_body(['calcn_',LR]);
    temp_ext_force.set_force_expressed_in_body('ground');
    temp_ext_force.set_point_expressed_in_body('ground');
    temp_ext_force.set_force_identifier(['ground_force_',Num_plate,'_v']);
    temp_ext_force.set_point_identifier(['ground_force_',Num_plate,'_p']);
    temp_ext_force.set_torque_identifier(['ground_moment_',Num_plate,'_m']);
    extLoadsObject.set(ind-1,temp_ext_force);
    
end
% my_model.addComponent(extLoadsObject)

myState =  my_model.initSystem();
%%

TS_count=0;
for TimeStep=1:nFrame_mot
    TS_count=TS_count+1;
    
    time_count(TS_count)=my_mot_imp.time(TimeStep);
    mot_count(TS_count,:)=my_mot_imp.data(TimeStep,:);
    
    % =======================update the motion file to the myState(TS)
%     for index_coor=1:nCoord_mot
%         myValue=my_mot_imp.data(TimeStep,index_coor);
%         %         my_model.setStateVariableValue(myState,my_mot_imp.colheaders(index_coor), myValue);
%         %         my_mot_imp.colheaders(index_coor)
%         my_coor_set.get(my_mot_imp.colheaders(index_coor)).setValue(myState, myValue)
%     end
    
    %  ======================= Muscles analysis ;
     my_model.equilibrateMuscles(myState);
%     my_model.realizeAcceleration(myState);
    
    torq_temp=f_vec2array(idSolver.solve(myState,myState.getUDot()))
    1
    
end


%%
torq_temp=f_vec2array(idSolver.solve(myState));