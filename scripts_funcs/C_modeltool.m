classdef C_modeltool < handle
    
    properties
        file_c3d
        file_trc
        file_trc_rot
        file_GRF
        
        %         file_grf
        c3d
        
        Subj
        condition
        
        
        OS
        step_length=1;
        res_mot
        res_ID
        res_ID_LM
        res_SO
        res_SO_grouped
        res_RRA_force
        res_analog
        res_EMG
        res_MVC
        res_Knee
        
        
        
        
        
        nFrame_mot
        nCoord_mot
        T_interest_ID
        T_interest_RRA
        state_musc
        state_MA_LM
        joints_interest
        
        folder_subj_osim
        folder_subj_trial
        folder_res_RRA
        
        
        file_osim_scaled
        file_osim_scaled_M
        file_osim_scaled_L
        file_osim_rra_adj
        file_mot
        file_setup_ExtLoads
        file_res_ID
        file_res_ID_M
        file_res_ID_L
        file_res_SO
        file_res_MVC
        file_state_musc
        file_setup_IK_SubjSpec
        file_setup_RRA
        file_state_MA_LM
        
        setup_forceplate_LR
        
        
        %         file_osim_scale_0=f_get_path_same_level('data_lfs\osim\1_FullBodyModel-4.0\Rajagopal2015_marker_0.osim');
        %         file_osim_scale_0=f_get_path_same_level('data_lfs\osim\3_FBLSmodel\FBLSmodel_OS4_PG_0.osim');
        %         file_osim_scale_0=f_get_path_same_level('data_lfs\osim\3_FBLSmodel\FBLSmodel_OS4_PG_2_lumber_rot_nonconst.osim');
        
        %         file_osim_scale_0=f_get_path_same_level('data_lfs\osim\3_FBLSmodel\FBLSmodel_OS4_PG_2_lumber_rot.osim');
        %
        %         file_setup_scale_0=f_get_path_same_level('data_lfs\dependent\my_setup_scale_0.xml');
        %         file_setup_IK_0=f_get_path_same_level('data_lfs\dependent\my_setup_IK_0.xml');
        %         file_setup_ID_0=f_get_path_same_level('data_lfs\dependent\my_setup_ID_0.xml');
        
        
        %         file_osim_scale_0=f_get_path_same_level('data_lfs\osim\3_FBLSmodel\FBLSmodel_OS4_PG_2_lumber_rot.osim');
        %         file_osim_scale_0=f_get_path_same_level('data_lfs\osim\gait2392_simbody_myAsics_0.osim');
        
        %         file_osim_scale_0=f_get_path_same_level('data_lfs\osim\gait2392_simbody_myAsics_1.osim');
        file_osim_scale_0=f_get_path_same_level('data_lfs\osim\gait2392_simbody_myAsics_1_6DOFKnee.osim');
        
        %         file_osim_scale_0=f_get_path_same_level('data_lfs\osim\3_FBLSmodel\FBLSmodel_OS4_PG_2_lumber_rot.osim');
        
        %         file_setup_scale_0=f_get_path_same_level('data_lfs\dependent\my_scale_TimSayer_v2.xml');
        file_setup_scale_0=f_get_path_same_level('data_lfs\dependent\my_scale_asics_v1.xml');
        %         file_setup_IK_0=f_get_path_same_level('data_lfs\dependent\my_IK_asics_v0.xml');
        file_setup_IK_0=f_get_path_same_level('data_lfs\dependent\my_IK_asics_v1.xml');
        file_setup_ID_0=f_get_path_same_level('data_lfs\dependent\my_setup_ID_0.xml');
        file_setup_RRAtasks_0=f_get_path_same_level('data_lfs\dependent\gait2392_RRA_Tasks_1.xml');
        
        
    end
    methods
        
        
        function obj = C_modeltool(Subj,condition,c3d_name)
            %             lumbar_1=C_lumbar_static_scale('001B','Static',70);
            %             [obj.file_c3d,obj.file_trc,obj.file_grf] = obj.ff_c3dID_to_filenames(c3d_session,c3d_name);
            [obj.file_c3d,obj.file_trc,obj.file_GRF] = obj.ff_c3dID_to_filenames(Subj.name,condition,c3d_name);
            obj.file_trc_rot=obj.ff_add_suffix(obj.file_trc,'rot');
            obj.Subj=Subj;
            obj.condition=condition;
            
            obj.folder_subj_osim=f_get_path_same_level(['data_lfs\subject_data\',Subj.name]);
            obj.ff_make_folder_ifnotexist(obj.folder_subj_osim);
            obj.file_osim_scaled=[obj.folder_subj_osim,'\',Subj.name,'.osim'];
            obj.file_osim_scaled_L=[obj.folder_subj_osim,'\',Subj.name,'_L.osim'];
            obj.file_osim_scaled_M=[obj.folder_subj_osim,'\',Subj.name,'_M.osim'];
            obj.file_osim_rra_adj=[obj.folder_subj_osim,'\',Subj.name,'_rra_adj.osim'];
            
            obj.folder_subj_trial=[obj.folder_subj_osim,'\',condition,'_',c3d_name];
            obj.ff_make_folder_ifnotexist(obj.folder_subj_trial);
            obj.file_mot=[obj.folder_subj_trial,'\res_mot.mot'];
            obj.file_setup_ExtLoads=[obj.folder_subj_trial,'\setup_ExtLoads.xml'];
            obj.file_res_ID=[obj.folder_subj_trial,'\res_ID.sto'];
            obj.file_res_ID_L=[obj.folder_subj_trial,'\res_ID_L.sto'];
            obj.file_res_ID_M=[obj.folder_subj_trial,'\res_ID_M.sto'];
            obj.file_res_SO=[obj.folder_subj_trial,'\res_SO.mat'];
            obj.file_res_MVC=[obj.folder_subj_osim,'\res_MVC.mat'];
            obj.folder_res_RRA=[obj.folder_subj_trial,'\res_RRA'];
            
            obj.file_state_MA_LM=[obj.folder_subj_trial,'\res_state_MA_LM.mat'];
            obj.file_state_musc=[obj.folder_subj_trial,'\res_state_musc.mat'];
            obj.file_setup_RRA=[obj.folder_subj_trial,'\setup_RRA.xml'];
            
            %             obj.file_setup_ExtLoads=[obj.folder_subj_trial,'\setup_ExtLoads.xml'];
        end
        
        
        
        function obj=f_load_MVC_c3d(obj,set_MVC,set_channelname)
            itf = actxserver('C3DServer.C3D');
            MVC_all=[];
            for n=1:length(set_MVC)
                temp_MVC=char(set_MVC(n));
                [temp_file_c3d_mvc,~,~] = obj.ff_c3dID_to_filenames(obj.Subj.name,obj.condition,temp_MVC);
                temp_file_c3d_mvc
                openc3d(itf, 0, temp_file_c3d_mvc);
                temp_analog = obj.ff_get_c3d_AnalogSet(itf);
                
                temp_EMG_raw=obj.ff_extract_EMG(temp_analog,set_channelname);
                temp_EMG_processed=obj.ff_process_EMG(temp_EMG_raw);
                MVC_all=[MVC_all;temp_EMG_processed];
            end
            
            obj.res_MVC.all=MVC_all;
            obj.res_MVC.max=max(MVC_all);
            
            res_MVC=obj.res_MVC;
            save(obj.file_res_MVC,'res_MVC');
            
        end
        
        function obj=f_load_MVC_mat(obj)     
            if exist(obj.file_res_MVC, 'file')
                load(obj.file_res_MVC)
                obj.res_MVC=res_MVC; 
                disp('res_MVC loaded...')
            else
                warning(['file_res_MVC',obj.file_res_MVC, 'not found']);
            end
        end
        
        
        
        
        function f_rotate_trc(obj,input_axis,angle_deg)
            trc=read_trcFile(obj.file_trc);
            trc_r=trc;
            expression=['r_temp=rot',input_axis,'(',num2str(angle_deg),');'];
            eval(expression);
            for n=1:size(trc.Data,2)/3
                indx_temp=(3:5)+3*(n-1);
                trc_r.Data(:,indx_temp)=trc.Data(:,indx_temp)*r_temp;
            end
            
            make_trcFile(obj.file_trc_rot,trc_r)
        end
        
        function f_rotate_shift_trc(obj,input_axis,angle_deg,vec3)
            trc=read_trcFile(obj.file_trc);
            trc_r=trc;
            expression=['r_temp=rot',input_axis,'(',num2str(angle_deg),');'];
            eval(expression);
            for n=1:size(trc.Data,2)/3
                indx_temp=(3:5)+3*(n-1);
                trc_r.Data(:,indx_temp)=(trc.Data(:,indx_temp)+vec3)*r_temp;
            end
            
            make_trcFile(obj.file_trc_rot,trc_r)
        end
        
        function obj=f_setup_scale(obj)
            import org.opensim.modeling.*;
            SCALEtool=ScaleTool(obj.file_setup_scale_0);
            
            SCALEtool.getGenericModelMaker.setModelFileName(obj.file_osim_scale_0);
            
            SCALEtool.setSubjectMass(obj.Subj.mass);
            SCALEtool.getModelScaler.setMarkerFileName(obj.file_trc_rot);
            SCALEtool.getMarkerPlacer.setMarkerFileName(obj.file_trc_rot);
            %
            SCALEtool.getModelScaler.setOutputModelFileName(obj.file_osim_scaled);
            SCALEtool.getMarkerPlacer.setOutputModelFileName(obj.file_osim_scaled);
            %             SCALEtool.run();
            
            file_scale_setup_out=[obj.folder_subj_osim,'\setup_Scale.xml'];
            SCALEtool.print(file_scale_setup_out);
            clear SCALEtool
            
            
            disp(' ')
            obj.ff_disp_and_copy(obj.file_osim_scale_0);
            %             disp('Osim_0 copied to clipboard.')
            %             clipboard('copy',obj.file_osim_scale_0);
            disp(file_scale_setup_out)
            
        end
        
        function obj=f_setup_osim_LM(obj,flag_PN)
            import org.opensim.modeling.*;
            
            % file_model='C:\OpenSim_4\model_1\Models\Gait2392_Simbody_Tim_Sayer\gait2392_simbody_TimSayer_0_JRF.osim';
            %             file_model='C:\Users\wuw4\Documents\GIT_local\lowerlimb_model_myasics\data_lfs\subject_data\Scott\Scott.osim';
            
            my_model=Model(obj.file_osim_scaled);
            KneeR=my_model.getJointSet.get('knee_r');
            
            f0=KneeR.get_frames(0);
            vec3_trans=f0.get_translation();
            vec3_trans_new=vec3_trans;
            
            lateral_trans=vec3_trans.get(1)/(-0.4)*0.025*flag_PN;
            
            vec3_trans_new.set(2,lateral_trans);
            f0.set_translation(vec3_trans_new);
            
            f1=KneeR.get_frames(1);
            vec3_trans=f1.get_translation();
            vec3_trans_new=vec3_trans;
            vec3_trans_new.set(2,lateral_trans);
            f1.set_translation(vec3_trans_new);
            
            %%
            if flag_PN>0
                file_output=obj.file_osim_scaled_L;
            else
                file_output=obj.file_osim_scaled_M;
            end
            
            my_model.print(file_output);
            disp(['OSIM saved to  ',file_output])
            
        end
        
        
        
        
        function obj=f_setup_IK(obj,flag_open_OpenSim)
            if flag_open_OpenSim
                
                if ~obj.ff_check_exe_running('OpenSim64.exe')
                    winopen('C:\OpenSim_4\bin\OpenSim64.exe');
                end
            end
            
            import org.opensim.modeling.*;
            IK_tool=InverseKinematicsTool(obj.file_setup_IK_0);
            
            IK_tool.setMarkerDataFileName(obj.file_trc_rot);
            
            
            IK_tool.setOutputMotionFileName(obj.file_mot);
            
            file_IK_setup_SubjSpec=[obj.folder_subj_trial,'\setup_IK.xml'];
            
            IK_tool.print(file_IK_setup_SubjSpec);
            clear IK_tool
            
            disp(' ')
            disp('========= Load scaled osim model =========')
            obj.ff_disp_and_copy(obj.file_osim_scaled);
            disp('========= Name of the IK setup file')
            disp(file_IK_setup_SubjSpec)
            disp('========= TRC file')
            disp(obj.file_trc_rot)
            disp('========= IK output')
            disp(obj.file_mot)
        end
        
        function obj=f_setup_ExtLoads(obj)
            import org.opensim.modeling.*;
            %             extLoadsObject = ExternalLoads('C:\Users\wuw4\OneDrive - The University of Melbourne\Documents\MATLAB\Tim_sayer\support_files\setup_id_api_no_model_no_external.xml');
            if isempty( obj.setup_forceplate_LR)
                warning('setup_forceplate_LR not setup');
                return
            end
               
            
            extLoadsObject = ExternalLoads();
            extLoadsObject.setName('ExLoad_API');
            extLoadsObject.setDataFileName(obj.file_GRF);
            extLoadsObject.setLowpassCutoffFrequencyForLoadKinematics(6);
            for ind=1:length(obj.setup_forceplate_LR)
                LR=obj.setup_forceplate_LR{ind}{1};
                Num_plate=obj.setup_forceplate_LR{ind}{2};
                
                temp_ext_force=ExternalForce;
                temp_ext_force.setName(['ex_force_',LR]);
                temp_ext_force.set_applied_to_body(['calcn_',LR]);
                temp_ext_force.set_force_expressed_in_body('ground');
                temp_ext_force.set_point_expressed_in_body('ground');
                temp_ext_force.set_force_identifier(['ground_force_',Num_plate,'_v']);
                temp_ext_force.set_point_identifier(['ground_force_',Num_plate,'_p']);
                temp_ext_force.set_torque_identifier(['ground_moment_',Num_plate,'_m']);
                extLoadsObject.set(ind-1,temp_ext_force);
            end
            extLoadsObject.print(obj.file_setup_ExtLoads);
            winopen(obj.file_setup_ExtLoads);
            clear extLoadsObject
            
        end
        function obj=f_load_model(obj)
            %             import org.opensim.modeling.*;
            %
            %             my_model=Model(obj.file_osim_scaled);
            %             myState =  my_model.initSystem();
            %             % ----muscle details
            %             my_musc_set=my_model.getMuscles();
            %             nMusc = my_musc_set.getSize();
            %             my_musc_names=cell(nMusc,1);
            %             for n=1:nMusc
            %                 my_musc_names(n)=my_musc_set.get(n-1).getName();
            %                 Lom(n)=my_musc_set.get(n-1).getOptimalFiberLength();
            %                 Fom(n)=my_musc_set.get(n-1).getMaxIsometricForce();
            %                 Lst(n)=my_musc_set.get(n-1).getTendonSlackLength();
            %                 Vmax(n)=Lom(n)*my_musc_set.get(n-1).getMaxContractionVelocity();
            %             end
            %
            %             my_coor_set=my_model.getCoordinateSet();
            %             nCoord_all = my_coor_set.getSize();
            %             my_coord_names=cell(nCoord_all,1);
            %             for n=1:nCoord_all
            %                 my_coord_names(n)=my_coor_set.get(n-1).getName();
            %             end
            %
            %
            %             obj.OS.model=my_model;
            %             obj.OS.set_musc=my_musc_set;
            %             obj.OS.name_musc= my_musc_names;
            %             obj.OS.name_coor= my_coord_names;
            %             obj.OS.nMusc=nMusc;
            %             obj.OS.state=myState;
            %             obj.OS.musc_Lom=Lom;
            %             obj.OS.musc_Lst=Lst;
            %             obj.OS.musc_Fom=Fom;
            %             obj.OS.musc_Vmax=Vmax;
            %             obj.OS.set_coor=my_coor_set;
            
            OS_temp=obj.ff_load_model(obj.file_osim_scaled);
            obj.OS=OS_temp;
            
        end
        
        
        function obj=f_setup_run_ID(obj,HZ_cutoff)
            import org.opensim.modeling.*;
            if isempty(obj.OS)
                f_load_model(obj);
            end
            IDtool=InverseDynamicsTool(obj.file_setup_ID_0);
            IDtool.setResultsDir(obj.folder_subj_trial);
            IDtool.setModel(obj.OS.model)
            IDtool.setCoordinatesFileName(obj.file_mot);
            if isempty(obj.res_mot)
                f_load_res_mot(obj);
            end
            IDtool.setStartTime(obj.res_mot.time(1));
            IDtool.setEndTime(obj.res_mot.time(end));
            IDtool.setLowpassCutoffFrequency(HZ_cutoff);
            
            IDtool.setOutputGenForceFileName('res_ID.sto');
            
            
            if exist(obj.file_setup_ExtLoads, 'file')
                IDtool.setExternalLoadsFileName(obj.file_setup_ExtLoads);
            end
            
            
            IDtool.print([obj.folder_subj_trial,'\setup_ID.xml']);
            IDtool.run();
            clear IDtool;
            
        end
        
        function obj=f_setup_run_ID_LM(obj,HZ_cutoff,flag_PN)
            import org.opensim.modeling.*;
            
            if flag_PN>0
                model_temp=Model(obj.file_osim_scaled_L);
                res_temp='res_ID_L.sto';
            else
                model_temp=Model(obj.file_osim_scaled_M);
                res_temp='res_ID_M.sto';
            end
            
            
            IDtool=InverseDynamicsTool(obj.file_setup_ID_0);
            IDtool.setResultsDir(obj.folder_subj_trial);
            IDtool.setModel(model_temp)
            IDtool.setCoordinatesFileName(obj.file_mot);
            if isempty(obj.res_mot)
                f_load_res_mot(obj);
            end
            IDtool.setStartTime(obj.res_mot.time(1));
            IDtool.setEndTime(obj.res_mot.time(end));
            IDtool.setLowpassCutoffFrequency(HZ_cutoff);
            
            IDtool.setOutputGenForceFileName(res_temp);
            
            
            if exist(obj.file_setup_ExtLoads, 'file')
                IDtool.setExternalLoadsFileName(obj.file_setup_ExtLoads);
            end
            
            
            %             IDtool.print([obj.folder_subj_trial,'\setup_ID.xml']);
            IDtool.run();
            clear IDtool;
            
        end
        
        
        function obj=f_setup_RRA(obj,range_time)
            import org.opensim.modeling.*
            % folder_res_RRA=f_get_path_same_level('data_lfs\dependent\gait2392_RRA_Actuators_0.xml');
            
            
            my_rra_0=RRATool();
            %             if isempty(obj.OS)
            %                 f_load_model(obj);
            %             end
            %             my_rra_0.setModel(obj.OS.model);
            
            my_rra_0.setModelFilename(obj.file_osim_scaled);
            my_rra_0.setReplaceForceSet(1);
            temp_ArrayStr=ArrayStr();
            temp_ArrayStr.set(0,f_get_path_same_level('data_lfs\dependent\gait2392_RRA_Actuators_1.xml'))
            my_rra_0.setForceSetFiles(temp_ArrayStr)
            my_rra_0.setResultsDir(obj.folder_res_RRA)
            
            my_rra_0.setTaskSetFileName(obj.file_setup_RRAtasks_0);
            %             my_rra_0.
            
            if isempty(obj.res_mot)
                f_load_res_mot(obj);
            end
            %             my_rra_0.setInitialTime(obj.res_mot.time(1));
            %             my_rra_0.setFinalTime(obj.res_mot.time(end));
            my_rra_0.setInitialTime(range_time(1));
            my_rra_0.setFinalTime(range_time(end));
            my_rra_0.setDesiredKinematicsFileName(obj.file_mot);
            
            
            if exist(obj.file_setup_ExtLoads, 'file')
                my_rra_0.setExternalLoadsFileName(obj.file_setup_ExtLoads);
            end
            my_rra_0.setAdjustCOMToReduceResiduals(1)
            my_rra_0.setOutputModelFileName(obj.file_osim_rra_adj);
            my_rra_0.setAdjustedCOMBody('torso');
            my_rra_0.setLowpassCutoffFrequency(6);
            
            my_rra_0.print(obj.file_setup_RRA);
            
            disp(' ');
            disp('RRA setup printed to: ');
            obj.ff_disp_and_copy(obj.file_setup_RRA)
        end
        
        function obj=f_run_RRA(obj)
            import org.opensim.modeling.*
            my_rra_1=RRATool(obj.file_setup_RRA);
            my_rra_1.run();
        end
        
        function obj=f_load_res_mot(obj)
            %             [obj.res_mot,obj.nFrame_mot,obj.nCoord_mot] = obj.ff_mot_read(obj.file_mot,time_pick);
            [obj.res_mot,obj.nFrame_mot,obj.nCoord_mot] = obj.ff_mot_read(obj.file_mot,[-1,1]*1e6);
        end
        
        function obj=f_load_res_ID(obj)
            if ~exist(obj.file_res_ID, 'file')
                obj.f_setup_run_ID(6)
            end
            
            obj.res_ID=importdata(obj.file_res_ID);
            obj.res_ID.colheaders(:,1) = [];
            obj.res_ID.time=obj.res_ID.data(:,1);
            obj.res_ID.data(:,1) = [];
        end
        
        function obj=f_load_res_ID_LM(obj)
            temp_imp_L=importdata(obj.file_res_ID_L);
            
            temp_index=ismember(temp_imp_L.colheaders,['knee_abd_r','_moment']);
            
            obj.res_ID_LM.time=temp_imp_L.data(:,1);
            obj.res_ID_LM.data(:,1) = temp_imp_L.data(:,temp_index);
            
            
            temp_imp_M=importdata(obj.file_res_ID_M);
            obj.res_ID_LM.data(:,2) = temp_imp_M.data(:,temp_index);
            
        end
        
        
        
        
        
        function obj=f_load_res_RRA(obj)
            
            file_force=[obj.folder_res_RRA,'\_Actuation_force.sto'];
            obj.res_RRA_force=importdata(file_force);
            obj.res_RRA_force.colheaders(:,1) = [];
            obj.res_RRA_force.time=obj.res_RRA_force.data(:,1);
            obj.res_RRA_force.data(:,1) = [];
        end
        
        function obj=f_load_res_ID_interest(obj)
            if isempty( obj.res_ID)
                obj.f_load_res_ID;
            end
            
            nCoor_SO=length(obj.joints_interest);
            T_interest_ID_tempf=[];
            header_tempf=cell(0);
            for n=1:nCoor_SO
                temp_index=find(ismember(obj.res_ID.colheaders,[char(obj.joints_interest(n)),'_moment']));
                if ~isempty(temp_index)
                    T_interest_ID_tempf=[T_interest_ID_tempf,obj.res_ID.data(:,temp_index) ];
                    header_tempf=[header_tempf,obj.joints_interest(n)];
                end
                
            end
            obj.T_interest_ID.data=T_interest_ID_tempf;
            obj.T_interest_ID.data_norm=T_interest_ID_tempf/(obj.Subj.mass);
            obj.T_interest_ID.header=header_tempf;
            obj.T_interest_ID.time=obj.res_ID.time;
            
            obj.T_interest_ID.data_filtered=obj.ff_filter_ID(T_interest_ID_tempf);
            
            disp('T_interest loaded...')
        end
        
        function obj=f_load_res_RRA_interest(obj)
            if isempty( obj.res_RRA_force)
                obj.f_load_res_RRA;
            end
            
            nCoor_SO=length(obj.joints_interest);
            T_interest_ID_tempf=[];
            header_tempf=cell(0);
            for n=1:nCoor_SO
                temp_index=find(ismember(obj.res_RRA_force.colheaders,[char(obj.joints_interest(n))]));
                if ~isempty(temp_index)
                    T_interest_ID_tempf=[T_interest_ID_tempf,obj.res_RRA_force.data(:,temp_index) ];
                    header_tempf=[header_tempf,obj.joints_interest(n)];
                end
                
            end
            obj.T_interest_RRA.data=T_interest_ID_tempf;
            obj.T_interest_RRA.header=header_tempf;
            obj.T_interest_RRA.time=obj.res_RRA_force.time;
        end
        
        
        function obj=f_get_MA_LM(obj,indx_act_muscle)
            
            if isempty(obj.res_mot)
                obj.f_load_res_mot;
            end
            
            OS_L=obj.ff_load_model(obj.file_osim_scaled_L);
            state_MA_L=obj.ff_get_MA(OS_L,'knee_abd_r',indx_act_muscle);
            
            OS_M=obj.ff_load_model(obj.file_osim_scaled_M);
            state_MA_M=obj.ff_get_MA(OS_M,'knee_abd_r',indx_act_muscle);
            temp_MA3d(:,:,1)=state_MA_L.MA;
            temp_MA3d(:,:,2)=state_MA_M.MA;
            
            state_MA_LM=state_MA_M;
            state_MA_LM.MA=temp_MA3d;
            
            save(obj.file_state_MA_LM,'state_MA_LM');
            obj.state_MA_LM=state_MA_LM;
        end
        
        function obj=f_load_MA_LM(obj)
            
            if exist(obj.file_state_MA_LM, 'file')
                load(obj.file_state_MA_LM)
                obj.state_MA_LM=state_MA_LM;
            else
                warning('file_state_musc not found, run f_get_MA_LM(indx_ML_muscle)');
            end
            
        end
        
        function obj=f_get_MA_Fvel(obj,indx_act_muscle)
            if isempty( obj.OS)
                obj.f_load_model;
            end
            if isempty(obj.res_mot)
                obj.f_load_res_mot;
            end
            if isempty(obj.joints_interest)
                disp('Please allocate the "joints_interest"');
                return
            end
            
            nMusc_pick=length(indx_act_muscle);
            musc_active=obj.OS.name_musc(indx_act_muscle);
            TS_count=0;
            
            for TimeStep=1:obj.step_length:obj.nFrame_mot
                TS_count=TS_count+1;
                disp(obj.res_mot.time(TimeStep))
                time_count(TS_count)=obj.res_mot.time(TimeStep);
                mot_count(TS_count,:)=obj.res_mot.data(TimeStep,:);
                
                % =======================update the motion file to the myState(TS)
                for index_coor=1:obj.nCoord_mot
                    myValue=obj.res_mot.data(TimeStep,index_coor);
                    obj.OS.set_coor.get(obj.res_mot.colheaders(index_coor)).setValue(obj.OS.state, myValue);
                    %                     my_coor_set.get(obj.res_mot.colheaders(index_coor)).setValue(myState, myValue)
                end
                
                %  ======================= Muscles analysis ;
                obj.OS.model.equilibrateMuscles(obj.OS.state);
                for index_mus=1:nMusc_pick
                    my_Lmt_temp(index_mus)=obj.OS.set_musc.get(char(musc_active(index_mus))).getLength(obj.OS.state);
                    %   ------------------------- Moment arm
                    for index_coor=1:length(obj.joints_interest)
                        coor_temp=obj.OS.set_coor.get(char(obj.joints_interest(index_coor)));
                        MA_temp= obj.OS.set_musc.get(char(musc_active(index_mus))).computeMomentArm(obj.OS.state,coor_temp);
                        my_MA_temp(index_mus,index_coor)=MA_temp;... % my_MA_temp=(muscle * coord)
                            my_MA(TS_count, index_mus,index_coor)= MA_temp; % my_MA=(timframe * muscle * coord)
                    end
                end
                my_Lmt(TS_count,:)=my_Lmt_temp;
            end
            my_Lmt_dot=diff(my_Lmt)/(time_count(2)-time_count(1));
            my_Lmt_dot=[my_Lmt_dot(1,:);my_Lmt_dot];
            my_Lmt_dot_norm=my_Lmt_dot./repmat(obj.OS.musc_Vmax(indx_act_muscle),size(my_Lmt_dot,1),1);
            
            
            
            state_musc_io.MA=my_MA;
            state_musc_io.time=time_count;
            state_musc_io.indx_musc=indx_act_muscle;
            state_musc_io.name_musc=obj.OS.name_musc(indx_act_muscle);
            state_musc_io.coor_MA=obj.joints_interest;
            state_musc_io.Lmt=my_Lmt;
            state_musc_io.Lmt_dot=my_Lmt_dot;
            state_musc_io.Lmt_dot_norm=my_Lmt_dot_norm;
            
            
            save(obj.file_state_musc,'state_musc_io');
            obj.state_musc=state_musc_io;
        end
        
        function obj=f_load_MA_Fvel(obj)
            
            if exist(obj.file_state_musc, 'file')
                load(obj.file_state_musc)
                obj.state_musc=state_musc_io;
                disp('MA loaded...')
            else
                warning('file_state_musc not found, run f_get_MA_Fvel(indx_act_muscle)');
            end
            
        end
        
        function f_GUI_mot(obj)
            if ~obj.ff_check_exe_running('OpenSim64.exe')
                winopen('C:\OpenSim_4\bin\OpenSim64.exe');
            end
            obj.ff_disp_and_copy(obj.file_osim_scaled);
            disp(obj.file_mot);
            
            if ~isempty(obj.file_GRF)
                disp(obj.file_GRF);
            end
            
        end
        
        function f_GUI_c3d(obj)
            
            
            if ~obj.ff_check_exe_running('Mokka.exe')
                winopen(f_get_path_same_level('data_lfs\Mokka\Mokka.exe'));
            end
            
            obj.ff_disp_and_copy(obj.file_c3d)
        end
        
        function obj=f_setup_run_SO(obj,flag_simple_force)
            options = optimset('Algorithm','interior-point','Display','off');
            nMusc_pick=length(obj.state_musc.indx_musc);
            
            if isempty(obj.OS)
                obj.f_load_model;
            end
            
            a_0=zeros(nMusc_pick,1);lb=a_0;
            a_1=ones(nMusc_pick,1);ub=a_1;
            
            
            for TimeStep=1:length(obj.state_musc.time)
                disp(['Timestep ',num2str(TimeStep),'/',num2str(length(obj.state_musc.time)), ' under optimization...']);
                my_Lmt_temp=obj.state_musc.Lmt(TimeStep,:);
                my_MA_temp=squeeze(obj.state_musc.MA(TimeStep, :,:));
                
                if length(obj.state_musc.coor_MA)==1
                    my_MA_temp=my_MA_temp';
                end
                
                my_Lmt_dot_temp=obj.state_musc.Lmt_dot_norm(TimeStep,:);
                
                %++++++++++++++++++++++++++++++++++++++++++++++++++++
                [F_act_alongtendon_temp,F_pas_alongtendon_temp]=obj.ff_act2force_my(a_1,my_Lmt_temp,my_Lmt_dot_temp,obj.state_musc.indx_musc);
                
                if flag_simple_force
                    F_pas_alongtendon_temp=zeros(1,nMusc_pick);
                    F_act_alongtendon_temp=obj.OS.musc_Fom(obj.state_musc.indx_musc);
                end
                
                F_act_alongtendon(TimeStep,:)=F_act_alongtendon_temp;
                F_pas_alongtendon(TimeStep,:)=F_pas_alongtendon_temp;
                
                time_temp=obj.state_musc.time(TimeStep);
                [~,ind_forID]=min(abs(obj.T_interest_ID.time-time_temp));
                T_interest_ID_temp=obj.T_interest_ID.data_filtered(ind_forID,:);
                T_interest_ID_TS(TimeStep,:)=T_interest_ID_temp;
                
                [a(TimeStep,:),fval(TimeStep),~]=fmincon(@obj.f_SO_obj,a_0,[],[],[],[],lb,ub,@(a)f_nonlcon(a,F_act_alongtendon_temp,F_pas_alongtendon_temp,my_MA_temp,T_interest_ID_temp),options);
                %                 [a(TS_count,:),fval(TS_count),exitflag]=fmincon(@f_SO_obj,a_0,[],[],[],[],lb,ub,@(a)f_nonlcon(a,F_act_alongtendon_temp,F_pas_alongtendon_temp,my_MA_temp,obj.T_interest_ID.data(TimeStep,:)),options);
                %                 [a(TimeStep,:),fval(TimeStep),~]=fmincon(@obj.f_SO_obj,a_0,[],[],[],[],lb,ub,[],options);
                Fm(TimeStep,:)=F_act_alongtendon_temp.*a(TimeStep,:)+F_pas_alongtendon_temp;
            end
            
            obj.res_SO.time=obj.state_musc.time;
            obj.res_SO.Fm=Fm;
            obj.res_SO.a=a;
            obj.res_SO.Tq=T_interest_ID_TS;
            obj.res_SO.F_pas=F_pas_alongtendon;
            obj.res_SO.F_act_max=F_act_alongtendon;
            obj.res_SO.name_musc=obj.state_musc.name_musc;

        end
        
        function obj=f_save_res_SO(obj)
            res_SO=obj.res_SO;
            save(obj.file_res_SO,'res_SO');
            disp(['file_res_SO saved to ',obj.file_res_SO]);            
        end
        
        function obj=f_load_res_SO(obj)
            
            if exist(obj.file_res_SO, 'file')
                load(obj.file_res_SO)
                obj.res_SO=res_SO;   
                disp('res_SO loaded...')
            else
                warning(['file_res_SO',obj.file_res_SO, 'not found']);
            end
        end        
        
        function obj=f_setup_run_KneeLoad(obj)
            
            
            f0=org.opensim.modeling.Model(obj.file_osim_scaled_L).getJointSet.get('knee_r').get_frames(0);
            r=abs(f0.get_translation().get(2)*2);
            
            f=obj.res_SO.Fm(:,obj.state_MA_LM.indx_musc);
            
            ma2d_L=squeeze(obj.state_MA_LM.MA(:,:,1));
            ma2d_M=squeeze(obj.state_MA_LM.MA(:,:,2));
            
            T_L_musc=sum(ma2d_L.*f,2);
            T_M_musc=sum(ma2d_M.*f,2);
            
            T_L_ID=obj.res_ID_LM.data(:,1);
            T_M_ID=obj.res_ID_LM.data(:,2);
            
            F_M_ID=(-T_L_ID)/r;
            F_L_ID=(T_M_ID)/r;
            
            F_M_musc=(T_L_musc)/r;
            F_L_musc=(-T_M_musc)/r;
            
            F_M_all=F_M_ID+F_M_musc;
            F_L_all=F_L_ID+F_L_musc;
            
            F_all_musc=F_M_musc+F_L_musc;
            F_all_ID=F_M_ID+F_L_ID;
            
            F_all=F_all_musc+F_all_ID;
            
            
            
            output_temp.time=obj.state_MA_LM.time;
            output_temp.F_all=F_all;
            output_temp.F_all_ID=F_all_ID;
            output_temp.F_all_musc=F_all_musc;
            output_temp.F_L_all=F_L_all;
            output_temp.F_M_all=F_M_all;
            output_temp.F_L_musc=F_L_musc;
            output_temp.F_M_musc=F_M_musc;
            output_temp.F_M_ID=F_M_ID;
            output_temp.F_L_ID=F_L_ID;
            output_temp.r=r;
            output_temp.T_L_musc=T_L_musc;
            output_temp.T_M_musc=T_M_musc;
            output_temp.T_L_ID=T_L_ID;
            output_temp.T_M_ID=T_M_ID;
            
            
            obj.res_Knee=output_temp;
            
            
            
            %%
            %             figure
            %             plot(Lb2.res_ID.time,[FL, F_M_ID, FL+F_M_ID])
            %
            %             legend('L','M')
            
        end
        
        function obj=f_fig_subplot_Knee(obj,flag_GRF_event)
            figure
            y_range=[-1000,1e4];
            
            t=obj.res_Knee.time;
                        
            subplot(2,3,1)
            plot(t,obj.res_Knee.F_all,'k','LineWidth',2);
            hold on
            plot(t,obj.res_Knee.F_all_musc,'k-');
            plot(t,obj.res_Knee.F_all_ID,'k--');
            legend('All','Muscle_all','ExtLoad_all','Interpreter','None')
            ylim(y_range);
            
            subplot(2,3,2)
            plot(t,obj.res_Knee.F_all_musc,'k-');
            hold on
            plot(t,obj.res_Knee.F_L_musc,'b-');
            plot(t,obj.res_Knee.F_M_musc,'r-');
            legend('Muscle','L_musc','M_musc','Interpreter','None')
            ylim(y_range);
            
            subplot(2,3,3)
            plot(t,obj.res_Knee.F_all_ID,'k--');
            hold on
            plot(t,obj.res_Knee.F_L_ID,'b--');
            plot(t,obj.res_Knee.F_M_ID,'r--');
            legend('ExtLoad','L_ext','M_ext','Interpreter','None')
            ylim(y_range);
            
            subplot(2,3,4)
            plot(t,obj.res_Knee.F_all,'k-','LineWidth',2);
            hold on
            plot(t,obj.res_Knee.F_L_all,'b-','LineWidth',2);
            plot(t,obj.res_Knee.F_M_all,'r-','LineWidth',2);
            legend('All','L_all','M_all','Interpreter','None')
            ylim(y_range);
            
            subplot(2,3,5)
            plot(t,obj.res_Knee.F_M_all,'r-','LineWidth',2);
            hold on
            plot(t,obj.res_Knee.F_M_musc,'r-')
            plot(t,obj.res_Knee.F_M_ID,'r--');
            legend('R_all','R_musc','R_ext','Interpreter','None')
            ylim(y_range);
            
            subplot(2,3,6)
            plot(t,obj.res_Knee.F_L_all,'b-','LineWidth',2);
            hold on
            plot(t,obj.res_Knee.F_L_musc,'b-');
            plot(t,obj.res_Knee.F_L_ID,'b--');
            legend('L_all','L_musc','L_ext','Interpreter','None')
            ylim(y_range);
            
            if flag_GRF_event
                obj.ff_plot_forceplate_shade(obj.setup_forceplate_LR,[2,3,6]);
            end
            
            
        end
        
        function obj=f_group_res_SO(obj,set_musc,set_lr)
            set_title=cell(0);
            Fm_grouped=[];
            a_grouped=[];
            F_act_max_grouped=[];
            F_pas_grouped=[];
            for n_musc=1:length(set_musc)
                temp_musc_name=char(set_musc(n_musc));
                flag_musc=contains(obj.state_musc.name_musc,temp_musc_name);
                
                for n_rl=1:length(set_lr)
                    flag_rl=contains(obj.state_musc.name_musc, char(set_lr(n_rl)));
                    temp_indx=find(flag_rl&flag_musc);
                    
                    temp_fm=obj.res_SO.F_act_max(:,temp_indx);
                    temp_fm_sum=sum(temp_fm,2);
                    F_act_max_grouped=[F_act_max_grouped,temp_fm_sum];
                    
                    temp_fm=obj.res_SO.F_pas(:,temp_indx);
                    temp_fm_sum=sum(temp_fm,2);
                    F_pas_grouped=[F_pas_grouped,temp_fm_sum];
                    
                    temp_fm=obj.res_SO.Fm(:,temp_indx);
                    temp_fm_sum=sum(temp_fm,2);
                    Fm_grouped=[Fm_grouped,temp_fm_sum];
                    
                    temp_a=obj.res_SO.a(:,temp_indx);
                    temp_fm_mean=mean(temp_a,2);
                    a_grouped=[a_grouped,temp_fm_mean];
                    
                    temp_title=[char(set_musc(n_musc)),char(set_lr(n_rl))];
                    set_title=[set_title,temp_title];
                end
            end
            
            
            obj.res_SO_grouped=obj.res_SO;
            
            obj.res_SO_grouped.Fm=Fm_grouped;
            obj.res_SO_grouped.a=a_grouped;
            obj.res_SO_grouped.name_musc=set_title;
            obj.res_SO_grouped.F_act_max=F_act_max_grouped;
            obj.res_SO_grouped.F_pas=F_pas_grouped;
        end
        
        
        function obj=f_fig_plot_T_interest_ID(obj)
            %             if isempty(obj.T_interest_ID)
            %                 obj.f_set_T_interest_ID;
            %             end
            
            figure;
            plot(obj.T_interest_ID.time,obj.T_interest_ID.data)
            legend(obj.T_interest_ID.header,'Interpreter', 'none')
            xlim([obj.T_interest_ID.time(1),obj.T_interest_ID.time(end)])
            xlabel('Time (s)');
            ylabel('Moment (Nm)');
        end
        
        
        function obj=f_fig_subplot_T_interest_ID(obj,flag_GRF_event)
            
            figure;
            subplot_layout=f_numSubplots(length(obj.T_interest_ID.header));
            range_ylim=[min(obj.T_interest_ID.data(:)),max(obj.T_interest_ID.data(:))]*1.08;
            
            for n=1:length(obj.T_interest_ID.header)
                subplot(subplot_layout(1),subplot_layout(2),n)
                plot(obj.T_interest_ID.time,obj.T_interest_ID.data(:,n))
                hold on;
                plot(obj.T_interest_ID.time,obj.T_interest_ID.data_filtered(:,n))
                
                title([num2str(n),'  ',char(obj.T_interest_ID.header(n))],'Interpreter', 'none');%'fontsize',10,
                xlim([obj.T_interest_ID.time(1),obj.T_interest_ID.time(end)])
                ylim(range_ylim)
                grid on;
                xlabel('Time (s)');
                ylabel('Moment (Nm)');
                
            end
            if flag_GRF_event
                obj.ff_plot_forceplate_shade(obj.setup_forceplate_LR,[subplot_layout,length(obj.T_interest_ID.header)]);
            end
        end
        
        function obj=f_fig_subplot_T_interest_ID_norm(obj,flag_GRF_event)
            
            figure;
            subplot_layout=f_numSubplots(length(obj.T_interest_ID.header));
            
            range_ylim=[min(obj.T_interest_ID.data_norm(:)),max(obj.T_interest_ID.data_norm(:))]*1.08;
            
            for n=1:length(obj.T_interest_ID.header)
                subplot(subplot_layout(1),subplot_layout(2),n)
                plot(obj.T_interest_ID.time,obj.T_interest_ID.data_norm(:,n));
                
                title([num2str(n),'  ',char(obj.T_interest_ID.header(n))],'Interpreter', 'none');%'fontsize',10,
                xlim([obj.T_interest_ID.time(1),obj.T_interest_ID.time(end)])
                ylim(range_ylim)
                grid on;
                xlabel('Time (s)');
                ylabel('Moment (Nm)');
                hold on
            end
            
            if flag_GRF_event
                obj.ff_plot_forceplate_shade(obj.setup_forceplate_LR,[subplot_layout,length(obj.T_interest_ID.header)]);
            end
        end

        function obj=f_fig_subplot_T_interest_RRA(obj)
            
            %             figure;
            subplot_layout=f_numSubplots(length(obj.T_interest_RRA.header));
            range_ylim=[min(obj.T_interest_RRA.data(:)),max(obj.T_interest_RRA.data(:))]*1.08;
            
            for n=1:length(obj.T_interest_RRA.header)
                subplot(subplot_layout(1),subplot_layout(2),n)
                %                 plot(obj.)
                plot(obj.T_interest_RRA.time,obj.T_interest_RRA.data(:,n))
                
                title([num2str(n),'  ',char(obj.T_interest_RRA.header(n))],'Interpreter', 'none');%'fontsize',10,
                xlim([obj.T_interest_RRA.time(1),obj.T_interest_RRA.time(end)])
                ylim(range_ylim)
                grid on;
                xlabel('Time (s)');
                ylabel('Moment (Nm)');
                hold on
            end
            
        end
        
        function obj=f_fig_subplot_res_Fm(obj,res_SO,flag_GRF_event)
            figure;
            subplot_layout=f_numSubplots(length(res_SO.name_musc));
            range_ylim=[0,max(res_SO.Fm(:))]*1.08;
            
            for n=1:length(res_SO.name_musc)
                subplot(subplot_layout(1),subplot_layout(2),n)
                plot(res_SO.time,res_SO.Fm(:,n))
                
                title([num2str(n),'  ',char(res_SO.name_musc(n))],'Interpreter', 'none');%'fontsize',10,
                xlim([res_SO.time(1),res_SO.time(end)])
                ylim(range_ylim)
                grid on;
                hold on;
            end
            if flag_GRF_event
            obj.ff_plot_forceplate_shade(obj.setup_forceplate_LR,[subplot_layout,length(res_SO.name_musc)])
            end
        end
        
        function obj=f_fig_subplot_res_a(obj,res_SO,flag_GRF_event)
            figure;
            subplot_layout=f_numSubplots(length(res_SO.name_musc));
            %             range_ylim=[0,max(res_SO.Fm(:))]*1.08;
            
            for n=1:length(res_SO.name_musc)
                subplot(subplot_layout(1),subplot_layout(2),n)
                plot(res_SO.time,res_SO.a(:,n))
                
                title([num2str(n),'  ',char(res_SO.name_musc(n))],'Interpreter', 'none');%'fontsize',10,
                xlim([res_SO.time(1),res_SO.time(end)])
                ylim([0 1])
                grid on;
                hold on;
            end
            
            if flag_GRF_event
                obj.ff_plot_forceplate_shade(obj.setup_forceplate_LR,[subplot_layout,length(res_SO.name_musc)])
            end
        end
        

        function obj=f_fig_subplot_res_a_EMG(obj,indx_musc_plot,flag_GRF_event,flag_norm)
            scale_EMG=1;
            
            if isempty(obj.res_SO)
                warning('No Static Optimz results, run or load SO first');
                return
            end
            if isempty(obj.res_EMG)
                warning('No EMG results, load and process EMG first');
                return
            end
            
            res_SO_temp=obj.res_SO;
            figure;
            subplot_layout=f_numSubplots(length(indx_musc_plot));
            
            for n=1:length(indx_musc_plot)
                temp_indx=indx_musc_plot(n);
                subplot(subplot_layout(1),subplot_layout(2),n)
                plot(res_SO_temp.time,res_SO_temp.a(:,temp_indx))
                hold on;
                title([num2str(temp_indx),'  ',char(res_SO_temp.name_musc(temp_indx))],'Interpreter', 'none');%'fontsize',10,
                %                 xlim([res_SO_temp.time(1),res_SO_temp.time(end)])
                xlim([res_SO_temp.time(1),res_SO_temp.time(end)])
                ylim([-0.5 1])
                
                
            end
            
            for n=1:length(obj.res_EMG.indx_OS_mucs)
                temp_indx_OS_musc=obj.res_EMG.indx_OS_mucs(n);
                
                ind_subplot=find(temp_indx_OS_musc==indx_musc_plot);
                if isempty(ind_subplot)
                    continue
                end
                subplot(subplot_layout(1),subplot_layout(2),ind_subplot);
                
                %                 plot(obj.res_EMG.time_EMG_adj,obj.res_EMG.EMG_norm(:,n))
                if flag_norm
                    plot(obj.res_EMG.time_EMG_adj,obj.res_EMG.EMG_norm(:,n));
                else
                    plot(obj.res_EMG.time_EMG_adj,obj.res_EMG.set_ACHANNEL_out(:,n)*scale_EMG-0.3);
                end
                %                 xlim([res_SO_temp.time(1),obj.res_EMG.time_EMG_adj(end)])
            end
            if flag_GRF_event
                obj.ff_plot_forceplate_shade(obj.setup_forceplate_LR,[subplot_layout,length(indx_musc_plot)]);
            end
        end
        
        function obj=f_fig_waterfall_res_Fm(obj,res_SO)
            figure;
            x=res_SO.time;
            y=1:length(res_SO.name_musc);
            [X,Y] = meshgrid(x,y);
            waterfall(X,Y,res_SO.Fm');
            set(gca,'ytick',1:length(res_SO.name_musc),'yticklabel',res_SO.name_musc,...
                'TickLabelInterpreter', 'none','YTickLabelRotation',90);
            zlabel('Force(N)');
            xlabel('Time(s)');
            
        end
        
        function obj=f_fig_waterfall_T_interest_ID(obj)
            %             if isempty(obj.T_interest_ID)
            %                 obj.f_set_T_interest_ID;
            %             end
            
            %             figure;
            %             subplot_layout=f_numSubplots(length(obj.T_interest_ID.header));
            %             range_ylim=[min(obj.T_interest_ID.data(:)),max(obj.T_interest_ID.data(:))]*1.08;
            %
            %             for n=1:length(obj.T_interest_ID.header)
            %                 subplot(subplot_layout(1),subplot_layout(2),n)
            %                 plot(obj.T_interest_ID.time,obj.T_interest_ID.data(:,n))
            %
            %                 title([num2str(n),'  ',char(obj.T_interest_ID.header(n))],'Interpreter', 'none');%'fontsize',10,
            %                 xlim([obj.T_interest_ID.time(1),obj.T_interest_ID.time(end)])
            %                 ylim(range_ylim)
            %                 grid on;
            %                 xlabel('Time (s)');
            %                 ylabel('Moment (Nm)');
            %
            %             end
            
            
            figure;
            x=obj.T_interest_ID.time;
            y=1:length(obj.T_interest_ID.header);
            [X,Y] = meshgrid(x,y);
            waterfall(X,Y,obj.T_interest_ID.data');
            set(gca,'ytick',1:length(obj.T_interest_ID.header),'yticklabel',obj.T_interest_ID.header,...
                'TickLabelInterpreter', 'none','YTickLabelRotation',90);
            zlabel('Force(N)');
            xlabel('Time(s)');
            
        end
        
        function obj=f_scale_Fom(obj,Scale_Fom)
            obj.OS.musc_Fom=obj.OS.musc_Fom*Scale_Fom;
        end
        
        
        %             obj.c3d.my_writeMOT_resample(obj.file_grf); % my_export all the GRF @ 120HZ
        %             f_replaceinfile('\-nan\(ind\)', '0', obj.file_grf);
        %             f_replaceinfile('nan', '0', obj.file_grf);
        %         end
        
        
        function f_writeTRC_Mokka(obj,flag_open_mokka)
            if flag_open_mokka
                
                if ~obj.ff_check_exe_running('Mokka.exe')
                    winopen(f_get_path_same_level('data_lfs\Mokka\Mokka.exe'));
                end
            end
            disp(' ')
            disp('==> Open Mokka, LOAD from:')
            
            
            obj.ff_disp_and_copy(obj.file_c3d)
            input('==> Press Enter to copy trc to clipboard...')
            disp(' ')
            disp(['==> Export trc to']);
            obj.ff_disp_and_copy(obj.file_trc)
            
            
            
            %             if isempty(obj.c3d)
            %                 obj.ff_load_rot_c3d();
            %             end
            %             obj.c3d.writeTRC(obj.file_trc);
            %             f_replaceinfile('\-nan\(ind\)', '0', obj.file_trc);
            %             f_replaceinfile('nan', '0', obj.file_trc);
            
        end
        
        function f_writeTRCGRF_OS(obj)
            
            C_c3d = osimC3D(obj.file_c3d,1);
            C_c3d.rotateData('x',-90);
            C_c3d.writeTRC(obj.file_trc_rot);
            C_c3d.writeMOT(obj.file_GRF);
            f_replaceinfile('\-nan\(ind\)', '0', obj.file_GRF);
            f_replaceinfile('nan', '0', obj.file_GRF);
            
        end
        function f_writeGRF_OS(obj)
            
            C_c3d = osimC3D(obj.file_c3d,1);
            C_c3d.rotateData('x',-90);
            %             C_c3d.writeTRC(obj.file_trc_rot);
            C_c3d.writeMOT(obj.file_GRF);
            f_replaceinfile('\-nan\(ind\)', '0', obj.file_GRF);
            f_replaceinfile('nan', '0', obj.file_GRF);
            
        end
        
        
        function f_extract_Analog(obj)
            % ------------Read-------------
            %     itf = c3dserver();
            itf = actxserver('C3DServer.C3D');
            openc3d(itf, 0, obj.file_c3d);
            obj.res_analog = obj.ff_get_c3d_AnalogSet(itf);
            
            
        end
        
        
        function EMG_norm=f_extract_proc_norm_EMG(obj,set_channelname)
            if isempty(obj.res_analog)
                obj.f_extract_Analog;
            end
            obj.res_EMG=obj.ff_extract_EMG(obj.res_analog,set_channelname);
            EMG_p=obj.ff_process_EMG(obj.res_EMG);
            obj.res_EMG.EMG_p=EMG_p;
            
            if isempty(obj.res_MVC)
                warning('No MVC loaded, skip EMG_norm');
            else
                EMG_norm=EMG_p./repmat(obj.res_MVC.max,size(EMG_p,1),1);
                obj.res_EMG.EMG_norm=EMG_norm;
            end
            
        end
        
        function f_fig_subplot_EMG(obj,flag_norm)
            
            N_channel=length(obj.res_EMG.set_channel_name);
            if flag_norm
                figure
                for n=1:N_channel
                    
                    
                    subplot(N_channel,1,n)
                    
                    plot(obj.res_EMG.time_EMG_adj,obj.res_EMG.EMG_norm(:,n));
                    hold on
                    plot(obj.res_EMG.time_EMG_adj,obj.res_EMG.set_ACHANNEL_out(:,n));
                    channelname=char(obj.res_EMG.set_channel_name(n));
                    title(channelname,'Interpreter','none');
                    max_data=max(obj.res_EMG.EMG_norm(:));
                    ylim([-0.5 max_data*1.1])
                end
                
            else
                
                figure
                for n=1:N_channel
                    subplot(N_channel,1,n)

                    plot(obj.res_EMG.time_EMG_adj,obj.res_EMG.EMG_p(:,n));
                    hold on
%                     plot(obj.res_EMG.time_EMG_adj,obj.res_EMG.set_ACHANNEL_out(:,n));
                    channelname=char(obj.res_EMG.set_channel_name(n));
                    title(channelname,'Interpreter','none');
                    max_data=max(obj.res_EMG.EMG_p(:));
                    ylim([0 max_data*1.1])
                    hold on
                    %                 xlim([obj.res_SO.time(1),obj.res_SO.time(end)])
                end
            end
        end
        
        
        function p=f_SO_obj(~,a)
            
            % p=sumsqr(a);
            % p=sum(a.*a.*a);
            p=sum(a.*a);
        end
        
        function [file_c3d,file_trc,file_grf] = ff_c3dID_to_filenames(obj,subj,condition,c3d_name)
            
            %             file_c3d=[f_get_path_same_level('data_lfs\c3d\'),subj,'\',condition,'\',subj,' ',c3d_name,'.c3d'];
            %             file_c3d=[f_get_path_same_level('data_lfs\c3d\'),subj,'\',condition,'\',condition,'_',c3d_name,'.c3d'];
            file_c3d=[f_get_path_same_level('data_lfs\c3d\'),subj,'\',condition,'\',condition,'01_',c3d_name,'.c3d'];
            
            folder_trcgrf=[f_get_path_same_level('data_lfs\trc_grf\'),subj,'\'];
            
            obj.ff_make_folder_ifnotexist(folder_trcgrf);
            
            file_trc=[folder_trcgrf,condition,'_',c3d_name,'.trc'];
            file_grf=[folder_trcgrf,condition,'_',c3d_name,'.mot'];
            
            %             file_c3d=[f_get_path_same_level('data_lfs\c3d\Subj_'),subj,'\',subj,' ',c3d_name,'.c3d'];
            %             file_trc=[f_get_path_same_level('data_lfs\trc\'),subj,' ',c3d_name,'.trc'];
            %
        end
        
        function  dataset_analog=ff_get_c3d_AnalogSet(~,itf)
            
            index1 = itf.GetVideoFrame(0); % frame start
            index2 = itf.GetVideoFrame(1); % frame end
            rf=itf.GetVideoFrameRate;
            r = double(itf.GetAnalogVideoRatio);
            
            
            nIndex = itf.GetParameterIndex('ANALOG', 'LABELS');
            nItems = itf.GetParameterLength(nIndex);
            
            set_channel_name={};
            set_ACHANNEL=[];
            for i = 1 : nItems
                channel_name = (itf.GetParameterValue(nIndex, i - 1));
                set_channel_name=[set_channel_name, channel_name];
                
                ACHANNEL_temp = itf.GetAnalogDataEx(i-1,index1,index2,'1',0,0,'0');
                ACHANNEL_temp = cell2mat(ACHANNEL_temp);
                set_ACHANNEL=[set_ACHANNEL, ACHANNEL_temp];
            end
            
            
            % ADDED BY TIM 12/6/08
            % Since this ACHANNEL value represents the analog values at the index1,
            % index2 VIDEO frames and they are inclusive, we must subtract (r-1) frames
            % from the end to represent the analog data frrom and including index1, to
            % the start of index2 (exclusive of it)
            l = length(ACHANNEL_temp);
            
            set_ACHANNEL_out= set_ACHANNEL((1:l-(r-1)),:);
            
            time_EMG=linspace(0,size(set_ACHANNEL_out,1)/r/rf,size(set_ACHANNEL_out,1))';
            
            time_EMG_adj=time_EMG-0.312;
            
            dataset_analog.set_ACHANNEL_out=set_ACHANNEL_out;
            dataset_analog.set_channel_name=set_channel_name;
            dataset_analog.time_EMG=time_EMG;
            dataset_analog.time_EMG_adj=time_EMG_adj;
            dataset_analog.ratio=itf.GetAnalogVideoRatio;
            dataset_analog.n_frame=itf.GetVideoFrameRate;
            
        end
        
        
        function  ff_make_folder_ifnotexist(~,folder_path_prod_output)
            if exist(folder_path_prod_output, 'dir')~=7
                mkdir(folder_path_prod_output)
            end
        end
        
        function  folder_output=ff_add_suffix(~,folder_input,suffix)
            [filepath,name,ext] = fileparts(folder_input);
            name=[name,'_',suffix];
            folder_output = [filepath,'\',name,ext];
        end
        
        function  flag_running=ff_check_exe_running(~,exe_name)
            [~,result] = system(['tasklist /FI "imagename eq ',exe_name,'" /fo table /nh']);
            flag_running = ~prod(result(1:8)=='INFO: No');
        end
        
        
        function  ff_disp_and_copy(~,expresion)
            disp([expresion,'  --(COPIED)--']);	clipboard('copy',expresion);
        end
        
        function [ F_act_alongtendon,F_pas_alongtendon ] = ff_act2force_my(obj,a,Lmt,lm_dot,indx_actvmuscle )
            nor_length=(Lmt-obj.OS.musc_Lst(indx_actvmuscle))./obj.OS.musc_Lom(indx_actvmuscle);
            l_temp=exp( -0.5.*((nor_length-1)/0.4).^2);
            Fv = 0.1433./ ( 0.1074+exp(-1.409.*sinh(3.2.*lm_dot+1.6 )));
            F_act_alongtendon=obj.OS.musc_Fom(indx_actvmuscle).*l_temp.*a'.*Fv;
            F_pas_alongtendon=obj.OS.musc_Fom(indx_actvmuscle).*exp(10.*(nor_length-1))./exp(5);
        end
        
        function OS=ff_load_model(~,file_model)
            import org.opensim.modeling.*;
            
            my_model=Model(file_model);
            myState =  my_model.initSystem();
            % ----muscle details
            my_musc_set=my_model.getMuscles();
            nMusc = my_musc_set.getSize();
            my_musc_names=cell(nMusc,1);
            for n=1:nMusc
                my_musc_names(n)=my_musc_set.get(n-1).getName();
                Lom(n)=my_musc_set.get(n-1).getOptimalFiberLength();
                Fom(n)=my_musc_set.get(n-1).getMaxIsometricForce();
                Lst(n)=my_musc_set.get(n-1).getTendonSlackLength();
                Vmax(n)=Lom(n)*my_musc_set.get(n-1).getMaxContractionVelocity();
            end
            
            my_coor_set=my_model.getCoordinateSet();
            nCoord_all = my_coor_set.getSize();
            my_coord_names=cell(nCoord_all,1);
            for n=1:nCoord_all
                my_coord_names(n)=my_coor_set.get(n-1).getName();
            end
            
            
            OS.model=my_model;
            OS.set_musc=my_musc_set;
            OS.name_musc= my_musc_names;
            OS.name_coor= my_coord_names;
            OS.nMusc=nMusc;
            OS.state=myState;
            OS.musc_Lom=Lom;
            OS.musc_Lst=Lst;
            OS.musc_Fom=Fom;
            OS.musc_Vmax=Vmax;
            OS.set_coor=my_coor_set;
            
        end
        
        
        function [my_mot_imp,nFrame_mot,nCoord_mot] = ff_mot_read(~,file_mot,time_pick)
            %             if nargin < 2
            %                 time_pick=[-1 1]*1e9;
            %             end
            
            % file_mot='temp3.mot';
            % file_mot='C:\OpenSim_4\model_1\Models\Gait2392_Simbody_Tim_Sayer\Subj_spec_model_data\Dev36\Barefoot\Dev36_Running_03_cut.mot';
            [my_mot_imp,~,~] = importdata(file_mot);
            
            if ~isfield(my_mot_imp,'colheaders')
                temp_colheaders=my_mot_imp.textdata{end};
                my_mot_imp.colheaders=strsplit(temp_colheaders,'\t');
            end
            
            
            my_mot_imp.colheaders=my_mot_imp.colheaders(2:end);
            
            time_all=my_mot_imp.data(:,1);
            
            indx_time=time_all>=time_pick(1)&time_all<=time_pick(2);
            my_mot_imp.data = my_mot_imp.data (indx_time,:);
            my_mot_imp.time=my_mot_imp.data(:,1);
            my_mot_imp.data(:,1) = [];
            my_mot_imp.data_deg= my_mot_imp.data;
            my_mot_imp.data= my_mot_imp.data/180*pi;
            
            [nFrame_mot, nCoord_mot] = size(my_mot_imp.data);
        end
        
        
        function output_EMG=ff_process_EMG(~,input_EMG)
            
            % Process
            output_EMG=double(input_EMG.set_ACHANNEL_out);
            output_EMG = detrend(output_EMG);
            
            %  'HPF'
            order = 2;
            freq = 25;
            [b_l,a_l] = butter(order, freq/(input_EMG.ratio*input_EMG.n_frame/2), 'high');
            output_EMG = filtfilt(b_l, a_l, output_EMG);
            
            % rectify
            output_EMG=abs(output_EMG);
            
            %  'LPF'
            order = 2;
            freq = 8;
            [b_l,a_l] = butter(order, freq/(input_EMG.ratio*input_EMG.n_frame/2), 'low');
            output_EMG = filtfilt(b_l, a_l, output_EMG);
        end
        function output_ID=ff_filter_ID(obj,input_IDdata)
            
            % Process
            %             output_ID=input_IDdata;
            
            
            order = 2;
            freq = 15;
            
            sample_rate=(length(obj.res_ID.time)-1)/(obj.res_ID.time(end)-obj.res_ID.time(1));
            [b_l,a_l] = butter(order, freq/(sample_rate/2), 'low');
            output_ID = filtfilt(b_l, a_l, input_IDdata);
        end
        
        
        function EMG_out=ff_extract_EMG(~,res_analog,set_channelname)
            
            EMG_out=res_analog;
            EMG_out.set_channel_name=[];
            EMG_out.set_ACHANNEL_out=[];
            
            for n=1:length(set_channelname)
                %                 channelname=char(set_channelname(n));
                channelname=set_channelname{n}{2};
                EMG_out.indx_OS_mucs(n)=set_channelname{n}{1};
                ind_temp=find(contains(res_analog.set_channel_name,channelname));
                EMG_out.set_ACHANNEL_out=[EMG_out.set_ACHANNEL_out,res_analog.set_ACHANNEL_out(:,ind_temp)];
                EMG_out.set_channel_name=[EMG_out.set_channel_name,res_analog.set_channel_name(ind_temp)];
            end
        end
        
        
        
        function ff_plot_forceplate_shade(obj,setup_forceplate_LR,subplot_layout_and_totalplot)
            
            if ~isempty(setup_forceplate_LR)
                imp_grf=importdata(obj.file_GRF);
            end
            for nn=1:length(setup_forceplate_LR)
                if setup_forceplate_LR{nn}{1}=='r'
                    temp_color='r';
                else
                    temp_color='b';
                end
                temp_ind=contains(imp_grf.colheaders,'vy')&contains(imp_grf.colheaders,setup_forceplate_LR{nn}{2});
                temp_grf_vy=imp_grf.data(:,temp_ind);
                %                     indx_nozero=find(temp_grf_vy>20);
                todraw=ones(size(temp_grf_vy))*10000;
                
                plotgtf_threshold=20;
                
                
                for n=1:subplot_layout_and_totalplot(end)
                    
                    subplot(subplot_layout_and_totalplot(1),subplot_layout_and_totalplot(2),n)
                    area(imp_grf.data(temp_grf_vy>plotgtf_threshold,1),todraw(temp_grf_vy>plotgtf_threshold),-10000,'FaceAlpha',0.1,'FaceColor',temp_color);
                end
                
                
            end
            
        end
        
        function state_musc_io=ff_get_MA(obj,OS,coordinate,indx_act_muscle)
            if isempty(obj.res_mot)
                obj.f_load_res_mot;
            end
            
            nMusc_pick=length(indx_act_muscle);
            musc_active=OS.name_musc(indx_act_muscle);
            TS_count=0;
            
            for TimeStep=1:obj.step_length:obj.nFrame_mot
                TS_count=TS_count+1;
                disp(obj.res_mot.time(TimeStep))
                time_count(TS_count)=obj.res_mot.time(TimeStep);
                mot_count(TS_count,:)=obj.res_mot.data(TimeStep,:);
                
                % =======================update the motion file to the myState(TS)
                for index_coor=1:obj.nCoord_mot
                    myValue=obj.res_mot.data(TimeStep,index_coor);
                    OS.set_coor.get(obj.res_mot.colheaders(index_coor)).setValue(OS.state, myValue);
                    %                     my_coor_set.get(obj.res_mot.colheaders(index_coor)).setValue(myState, myValue)
                end
                
                %  ======================= Muscles analysis ;
                OS.model.equilibrateMuscles(OS.state);
                for index_mus=1:nMusc_pick
                    my_Lmt_temp(index_mus)=OS.set_musc.get(char(musc_active(index_mus))).getLength(OS.state);
                    %   ------------------------- Moment arm
                    
                    coor_temp=OS.set_coor.get(coordinate);
                    MA_temp= OS.set_musc.get(char(musc_active(index_mus))).computeMomentArm(OS.state,coor_temp);
                    
                    my_MA(TS_count, index_mus)= MA_temp; % my_MA=(timframe * muscle )
                    
                end
                
            end
            state_musc_io.MA=my_MA;
            state_musc_io.time=time_count;
            state_musc_io.mot=mot_count;
            state_musc_io.indx_musc=indx_act_muscle;
            state_musc_io.name_musc=OS.name_musc(indx_act_muscle);
        end
        
        
    end
    
    methods(Static)
        
        
        function f_copyNexusC3D_2_Repo(folder_nexus,subj,condition)  % copy c3d from Nexus to Repo
            folder_nexus_ext=[folder_nexus,'*.c3d'];
            listing = dir(folder_nexus_ext);
            folder_dest=[f_get_path_same_level('data_lfs\c3d\'),subj.name,'\',condition];
            if exist(folder_dest, 'dir')~=7
                mkdir(folder_dest)
            end
            for n=1:length(listing )
                temp_file=[listing(n).folder,'\',listing(n).name];
                copyfile(temp_file,folder_dest);
            end
            
            disp(['C3D copied to ',folder_dest,'(COPIED)']);
            clipboard('copy',folder_dest);
            
            
        end
    end
    
end