% ================= EMG Processing
if 0
    EMG_p=double(EMG);
    figure;plot (EMG_p)
    
    % removeDC
    EMG_p = detrend(EMG_p);
    
    %  'HPF'
    order = 2;
    freq = 25;
    [b_l,a_l] = butter(order, freq/(ratio*n_frame/2), 'high');
    EMG_p = filtfilt(b_l, a_l, EMG_p);
    
    % rectify
    EMG_p=abs(EMG_p);
    
    %  'LPF'
    order = 2;
    freq = 2;
    [b_l,a_l] = butter(order, freq/(ratio*n_frame/2), 'low');
    EMG_p = filtfilt(b_l, a_l, EMG_p);
    
    figure;plot (EMG_p)
end