if 1
    %     import org.opensim.modeling.*
    %
    %     global my_model my_musc_set my_musc_names my_coord_names...
    %         nMusc myState Lom Lst Fom Vmax
    %
    %     %%
    %
    %     flag_EMG=0;
    %     flag_picktime=0;
    %     flag_simple_force=1; % 1 ideal actuator, 0 Hill-type model
    %     Scale_Fom=1;
    %
    %     step_length=1;
    %
    %     able_side='l';
    %     Joints_for_MA={...
    %         'knee_angle_abd_r';...
    %         };
    %
    %     nCoor_MA=length(Joints_for_MA);
    %     %%
    %     save_flag=1;
    %     result_fold='\results\XXX\';
    %
    %     %%
    %
    %     File_model='C:\OpenSim_4\model_1\Models\Gait2392_Simbody_Tim_Sayer\gait2392_simbody_TimSayer_0_JRF_PG1.osim';
    %     File_motion=file_IK_mot_out;
    %     File_GRF=file_grf;
    %
    %
    %     options = optimset('Algorithm','interior-point','Display','off');
    %     %%   Load model
    % %     disp('Loading model')
    % %
    % %     my_model=Model(File_model);
    % %     myState =  my_model.initSystem();
    % %
    % %     % ----muscle details
    % %     my_musc_set=my_model.getMuscles();
    % %     nMusc = my_musc_set.getSize();
    % %     my_musc_names=cell(nMusc,1);
    % %     for n=1:nMusc
    % %         my_musc_names(n)=my_musc_set.get(n-1).getName();
    % %         Lom(n)=my_musc_set.get(n-1).getOptimalFiberLength();
    % %         Fom(n)=my_musc_set.get(n-1).getMaxIsometricForce();
    % %         Lst(n)=my_musc_set.get(n-1).getTendonSlackLength();
    % %         Vmax(n)=Lom(n)*my_musc_set.get(n-1).getMaxContractionVelocity();
    % %     end
    % %     if exist('disable_muscle_ind','var')
    % %         Fom(disable_muscle_ind)=0;
    % %     end
    % %     Fom=Fom*Scale_Fom;
    % %
    % %     indx_actvmuscle=[44:86];
    % %     indx_actvmuscle=[1:92];
    % %     nMusc_pick=length(indx_actvmuscle);
    % %     musc_active=my_musc_names(indx_actvmuscle);
    % %     % ----coord details
    % %     my_coor_set=my_model.getCoordinateSet();
    % %     nCoord_all = my_coor_set.getSize();
    % %     my_coord_names=cell(nCoord_all,1);
    % %     for n=1:nCoord_all
    % %         my_coord_names(n)=my_coor_set.get(n-1).getName();
    % %     end
    % %
    % %
    %     %%  Obtain kinematics data-----------
    %
    %     time_pick=[1.6, 2.5];
    %     time_pick=[0, 100];
    %     disp('Obtaining kinematics data...')
    %
    %     [my_mot_imp,delimiterOut,headerlinesOut] = importdata(File_motion);
    %     my_mot_imp.colheaders=my_mot_imp.colheaders(2:end);
    %     time_all=my_mot_imp.data(:,1);
    %     indx_time=time_all>=time_pick(1)&time_all<=time_pick(2);
    %     my_mot_imp.data = my_mot_imp.data (indx_time,:);
    %
    %     time=my_mot_imp.data(:,1);
    %
    %     my_mot_imp.data(:,1) = [];
    %
    %     my_mot_imp.data=my_mot_imp.data/180*pi;
    %     [nFrame_mot, ~] = size(my_mot_imp.data);
    %     nCoord_mot=length(my_mot_imp.colheaders);
    %
    %
    
    
    %% Pre-run the Loop to get muscle moment arm, velocity, etc.--------------
    Joints_for_MA={...
        'knee_angle_abd_r';...
        };
    
    nCoor_MA=length(Joints_for_MA);
    TS_count=0;
    
    for TimeStep=1:step_length:nFrame_mot
        TS_count=TS_count+1;
        
        time_count(TS_count)=time(TimeStep);
        mot_count(TS_count,:)=my_mot_imp.data(TimeStep,:);
        
        
        %  ======================= Muscles analysis ;
        my_model.equilibrateMuscles(myState);
        for index_mus=1:nMusc_pick
            %   ------------------------- Moment arm
            for index_coor=1:1:nCoor_MA
                coor_temp=my_coor_set.get(char(Joints_for_MA(index_coor)));
                MA_temp= my_musc_set.get(char(musc_active(index_mus))).computeMomentArm(myState,coor_temp);
                
                my_MA_shift(TS_count, index_mus,index_coor)= MA_temp; % my_MA=(timframe * muscle * coord)
            end
        end
        
    end
end