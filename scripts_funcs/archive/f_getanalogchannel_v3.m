function [dataset_analog] = f_getanalog_set(itf)
% GETANALOGCHANNEL - returns nx1 array containing analog data.  The
% returned analog data is scaled with offsets removed, but is in the force plate
% coordinate system.
%
%   USAGE:  ACHANNEL = getanalogchannel(itf, signalname, index1*, index2*)
%           * = not a necessary input
%   INPUTS:
%   itf         = variable name used for COM object
%   signalname  = string name of desired signal
%   index1      = start frame index, all frames if not used as an argument
%   index2      = end frame index, all frames if not used as an argument
%   OUTPUTS:
%   ACHANNEL    = nx1 matrix of analog data


index1 = itf.GetVideoFrame(0); % frame start
index2 = itf.GetVideoFrame(1); % frame end
rf=itf.GetVideoFrameRate;
r = double(itf.GetAnalogVideoRatio);


nIndex = itf.GetParameterIndex('ANALOG', 'LABELS');
nItems = itf.GetParameterLength(nIndex);

set_channel_name={};
set_ACHANNEL=[];
for i = 1 : nItems
    channel_name = (itf.GetParameterValue(nIndex, i - 1));
    set_channel_name=[set_channel_name, channel_name];
    
    ACHANNEL_temp = itf.GetAnalogDataEx(i-1,index1,index2,'1',0,0,'0');
    ACHANNEL_temp = cell2mat(ACHANNEL_temp);
    set_ACHANNEL=[set_ACHANNEL, ACHANNEL_temp];
end


% ADDED BY TIM 12/6/08
% Since this ACHANNEL value represents the analog values at the index1,
% index2 VIDEO frames and they are inclusive, we must subtract (r-1) frames
% from the end to represent the analog data frrom and including index1, to
% the start of index2 (exclusive of it)
l = length(ACHANNEL_temp);

set_ACHANNEL_out= set_ACHANNEL((1:l-(r-1)),:);

time_EMG=linspace(0,size(set_ACHANNEL_out,1)/r/rf,size(set_ACHANNEL_out,1))';

time_EMG_adj=time_EMG+0.312;

dataset_analog.set_ACHANNEL_out=set_ACHANNEL_out;
dataset_analog.set_channel_name=set_channel_name;
dataset_analog.time_EMG=time_EMG;
dataset_analog.time_EMG_adj=time_EMG_adj;

end