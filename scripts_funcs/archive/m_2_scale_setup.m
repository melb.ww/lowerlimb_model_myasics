clear all
close all
import org.opensim.modeling.*


subject_='Dev24';
condition_='Barefoot';
condition_='Kayano';
condition_='Zaraca';
% filename_input=[subject_,' Cal 01'];
% filename_input=[subject_,' Running 08'];
% filename_input=[subject_,' Running 09'];

out_folder_path_root='C:\OpenSim_4\model_1\Models\Gait2392_Simbody_Tim_Sayer\Subj_spec_model_data\';

folder_path_prod_output=fullfile(out_folder_path_root,subject_,condition_);
% filename_=strrep(filename_input, ' ', '_');

% file_trc=fullfile(folder_path_prod_output,[filename_,'.trc']);
file_trc_cali=fullfile(folder_path_prod_output,'cali.trc');
% file_grf=fullfile(folder_path_prod_output,[filename_,'_grf.mot']);
% file_emg=fullfile(folder_path_prod_output,[filename_,'_EMG.mat']);
file_ID_setup=fullfile(folder_path_prod_output,[subject_,'_',condition_,'_scale_setup.xml']);

% file_osim=[fullfile(out_folder_path_root,subject_),'\',subject_,'_scaled.osim'];
file_osim=fullfile(folder_path_prod_output,[subject_,'_',condition_,'_scaled.osim']);


SCALEtool=ScaleTool('C:\OpenSim_4\model_1\Models\Gait2392_Simbody_Tim_Sayer\my_scale_TimSayer_v2.xml');

SCALEtool.getModelScaler.setMarkerFileName(file_trc_cali);
SCALEtool.getMarkerPlacer.setMarkerFileName(file_trc_cali);

SCALEtool.getModelScaler.setOutputModelFileName(file_osim);
SCALEtool.getMarkerPlacer.setOutputModelFileName(file_osim);

SCALEtool.print(file_ID_setup);

disp(file_ID_setup);